<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name', 'Veterinaria') }}</title>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Fonts -->
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/fontawesome/css/all.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
</head>
<body class="content-white">
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    Veterinaria
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                    {{ __('Cerrar Session') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <main class="col-md-12 py-4">
            <div class="row">
                <div class="col-3">
                    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <a class="nav-link @if(Request::segment(1) == ''){{ __('active') }}@endif" href="{{ route('home') }}">
                            <span class="fas fa-home mr-10"></span>
                            {{ __('Home') }}
                        </a>
                        @if (Auth::user()->rol == 'A')
                        <a class="nav-link dropdown-toggle @if(Request::segment(1) == 'admin'){{ __('active') }}@endif" href="#adminMenu" data-toggle="collapse" aria-expanded="false">
                            <span class="fas fa-users-cog mr-10"></span>
                            {{ __('Administrador') }}
                        </a>
                        <ul class="collapse list-unstyled " id="adminMenu">
                            <li>
                                <a class="nav-link @if(Request::segment(2) == 'usuarios'){{ __('active') }}@endif" href="{{ route('usuarios') }}">
                                    <span class="fas fa-user mr-10"></span>
                                    {{ __('Usuarios') }}
                                </a>
                            </li>
                            <li>
                                <a class="nav-link @if(Request::segment(2) == 'veterinarios'){{ __('active') }}@endif" href="{{ route('veterinarios') }}">
                                    <span class="fas fa-users mr-10"></span>
                                    {{ __('Veterinarios') }}
                                </a>
                            </li>
                            <li>
                                <a class="nav-link @if(Request::segment(2) == 'pacientes'){{ __('active') }}@endif" href="{{ route('pacientes') }}">
                                    <span class="fas fa-dog mr-10"></span>
                                    {{ __('Pacientes') }}
                                </a>
                            </li>
                            <li>
                                <a class="nav-link @if(Request::segment(2) == 'citas'){{ __('active') }}@endif" href="{{ route('citas') }}">
                                    <span class="fas fa-calendar-plus mr-10"></span>
                                    {{ __('Agendar Cita') }}
                                </a>
                            </li>
                        </ul>
                        @else
                        <a class="nav-link dropdown-toggle @if(Request::segment(1) == 'veterinario'){{ __('active') }}@endif" href="#veterinarioMenu" data-toggle="collapse" aria-expanded="false">
                            <span class="fas fa-user-md mr-10"></span>
                            {{ __('Veterinario') }}
                        </a>
                        <ul class="collapse list-unstyled " id="veterinarioMenu">
                            <li>
                                <a class="nav-link @if(Request::segment(2) == 'atenciones'){{ __('active') }}@endif" href="{{ route('atenciones') }}">
                                    <span class="fas fa-calendar-check mr-10"></span>
                                    {{ __('Atenciones Pendientes') }}
                                </a>
                            </li>
                        </ul>
                        @endif
                    </div>
                </div>
                <div class="col-9">
                    @yield('content')
                </div>
            </div>
        </main>
    </div>
</body>
    <script src="{{ asset('assets/js/jquery-3.4.1.min.js') }}" defer></script>
    <script src="{{ asset('assets/js/bootstrap.min.js') }}" defer></script>
</html>
