@extends('layouts.appIn')

@section('content')

<form method="POST" action="@if ($Usuario){{ route('usuarios/edit', $Usuario->id) }}@else{{ route('usuarios') }}@endif">
    @csrf
    <div class="row">
        <div class="col-6">
            <div class="input-group form-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-user mx-auto"></i></span>
                </div>
                <input type="text" name="name" class="form-control @error('nombre') is-invalid @enderror" value="@if ($Usuario){{ $Usuario->name }}@endif" placeholder="nombre" autofocus required>  
            </div>
            @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="col-6">
            <div class="input-group form-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-user mx-auto"></i></span>
                </div>
                <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" value="@if ($Usuario){{ $Usuario->email }}@endif" placeholder="Email" autofocus required>  
            </div>
            @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="col-6">
            <div class="input-group form-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-user mx-auto"></i></span>
                </div>
                <select id="rol" type="text" class="form-control @error('rol') is-invalid @enderror" value="@if ($Usuario){{ $Usuario->rol }}@endif" name="rol" required>
                    <option value="0">Escoja un rol</option>
                    <option value="A">Administrador</option>
                    <option value="V">Veterinario</option>
                </select>
            </div>
            @error('rol')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="col-6">
            <div class="input-group form-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-user mx-auto"></i></span>
                </div>
                <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" placeholder="Contraseña" required>  
            </div>
            @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="col-6">
            <div class="input-group form-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-user mx-auto"></i></span>
                </div>
                <input type="password" name="password_confirmation" class="form-control @error('password_confirmation') is-invalid @enderror" placeholder="Confima tu contraseña" required>  
            </div>
            @error('password_confirmation')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="col-12">
            @if ($Usuario)
            <div class="form-group">
                <button type="submit" class="btn float-right login_in_btn">
                    {{ __('Actualizar') }}
                </button>
                <a class="btn float-right login_in_btn mr-15" href="{{ route('usuarios') }}">
                    {{ __('Nuevo') }}
                </a>
            </div>
            @else
            <div class="form-group">
                <button type="submit" class="btn float-right login_in_btn">
                    {{ __('Guardar') }}
                </button>
            </div>
            @endif
        </div>
    </div>
</form>

<div class="row mt-20">
    <div class="col-sm-12">
        <table class="table table-condensed table-striped">
            <thead>
                <tr>
                    <th>id</th>
                    <th>Nombre</th>
                    <th>Rol</th>
                    <th>Email</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($usuarios as $usuario)
                    <tr>
                        <td>{{ $usuario->id }}</td>
                        <td>{{ $usuario->name }}</td>
                        <td>@if ($usuario->rol == 'A'){{ __('Administrador') }}@else{{ __('Veterinario') }}@endif</td>
                        <td>{{ $usuario->email }}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="6">No hay registros.</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
</div>
@if ($msg)
    <script>alert('{{ $msg }}')</script>
@endif
@endsection
