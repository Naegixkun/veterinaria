@extends('layouts.appIn')

@section('content')

<form method="POST" action="@if ($Veterinario){{ route('veterinarios/edit', $Veterinario->Codigo) }}@else{{ route('veterinarios') }}@endif">
    @csrf
    <div class="row">
        <div class="col-6">
            <div class="input-group form-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-user-md mx-auto"></i></span>
                </div>
                <input type="text" name="nombres" class="form-control @error('nombres') is-invalid @enderror" value="@if ($Veterinario){{ $Veterinario->Nombres }}@endif" placeholder="nombres" autofocus required>  
            </div>
            @error('nombres')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
        <div class="col-6">
            <div class="input-group form-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-user-md mx-auto"></i></span>
                </div>
                <input type="text" name="apellidos" class="form-control @error('apellidos') is-invalid @enderror" value="@if ($Veterinario){{ $Veterinario->Apellidos }}@endif" placeholder="apellidos" required>  
            </div>
            @error('apellidos')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
        <div class="col-6">
            <div class="input-group form-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-calendar mx-auto"></i></span>
                </div>
                <input type="date" name="fechaNacimiento" class="form-control @error('fechaNacimiento') is-invalid @enderror" value="@if ($Veterinario){{ $Veterinario->FechaNacimiento }}@endif" placeholder="fecha de nacimiento" required>  
            </div>
            @error('fechaNacimiento')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
        <div class="col-6">
            <div class="input-group form-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-surprise mx-auto"></i></span>
                </div>
                <input type="text" name="especialidad" class="form-control @error('especialidad') is-invalid @enderror" value="@if ($Veterinario){{ $Veterinario->Especialidad }}@endif" placeholder="especialidad" required>  
            </div>
            @error('especialidad')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
        <div class="col-6">
            <div class="input-group form-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-surprise mx-auto"></i></span>
                </div>
                <select name="user" class="form-control @error('user') is-invalid @enderror" required>
                    <option value="">Asigne un usuario</option>
                    @forelse ($usuarios as $usuario)
                        <option value="{{ $usuario->id }}" @if ($Veterinario && $Veterinario->usuario == $usuario->id){{ __('selected') }}@endif>{{ $usuario->name }}</option>
                    @empty
                    @endforelse
                </select>
            </div>
            @error('user')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
        <div class="col-12">
            @if ($Veterinario)
            <div class="form-group">
                <button type="submit" class="btn float-right login_in_btn">
                    {{ __('Actualizar') }}
                </button>
                <a class="btn float-right login_in_btn mr-15" href="{{ route('veterinarios') }}">
                    {{ __('Nuevo') }}
                </a>
            </div>
            @else
            <div class="form-group">
                <button type="submit" class="btn float-right login_in_btn">
                    {{ __('Guardar') }}
                </button>
            </div>
            @endif
        </div>
    </div>
</form>

<div class="row mt-20">
    <div class="col-sm-12">
        <table class="table table-condensed table-striped">
            <thead>
                <tr>
                    <th>Codigo</th>
                    <th>Nombres</th>
                    <th>Apellidos</th>
                    <th>Fecha Nacimiento</th>
                    <th>Especialidad</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($veterinarios as $veterinario)
                    <tr>
                        <td>{{ $veterinario->Codigo }}</td>
                        <td>{{ $veterinario->Nombres }}</td>
                        <td>{{ $veterinario->Apellidos }}</td>
                        <td>{{ $veterinario->FechaNacimiento }}</td>
                        <td>{{ $veterinario->Especialidad }}</td>
                        <td>
                            <a class="btn btn-primary mr-15" href="{{ route('veterinarios/edit', $veterinario->Codigo) }}">
                                <span class="fas fa-edit mx-auto"></span>
                            </a>
                            <a class="btn btn-danger text-white" href="{{ route('veterinarios/delete', $veterinario->Codigo) }}">
                                <span class="fas fa-trash mx-auto"></span>
                            </a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="6">No hay registros.</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
</div>
@endsection
