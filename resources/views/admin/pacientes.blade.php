@extends('layouts.appIn')

@section('content')

<form method="POST" action="@if ($Paciente){{ route('pacientes/edit', $Paciente->Codigo) }}@else{{ route('pacientes') }}@endif">
    @csrf
    <div class="row">
        <div class="col-6">
            <div class="input-group form-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-dog mx-auto"></i></span>
                </div>
                <input type="text" name="nombre" class="form-control @error('nombre') is-invalid @enderror" value="@if ($Paciente){{ $Paciente->Nombre }}@endif" placeholder="nombre" autofocus required>  
            </div>
            @error('nombre')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="col-6">
            <div class="input-group form-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-user mx-auto"></i></span>
                </div>
                <input type="text" name="propietario" class="form-control @error('propietario') is-invalid @enderror" value="@if ($Paciente){{ $Paciente->Propietario }}@endif" placeholder="propietario" autofocus required>  
            </div>
            @error('propietario')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="col-6">
            <div class="input-group form-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-home mx-auto"></i></span>
                </div>
                <input type="text" name="direccion" class="form-control @error('direccion') is-invalid @enderror" value="@if ($Paciente){{ $Paciente->Direccion }}@endif" placeholder="dirección" autofocus required>  
            </div>
            @error('direccion')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="col-6">
            <div class="input-group form-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-hashtag mx-auto"></i></span>
                </div>
                <input type="number" name="telefono" class="form-control @error('telefono') is-invalid @enderror" value="@if ($Paciente){{ $Paciente->Telefono }}@endif" placeholder="telefono" autofocus required>  
            </div>
            @error('telefono')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="col-6">
            <div class="input-group form-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-calendar mx-auto"></i></span>
                </div>
                <input type="date" name="fechaNacimiento" class="form-control @error('fechaNacimiento') is-invalid @enderror" value="@if ($Paciente){{ $Paciente->FechaNacimiento }}@endif" placeholder="fecha de nacimiento" autofocus required>  
            </div>
            @error('fechaNacimiento')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="col-6">
            <div class="input-group form-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-paw mx-auto"></i></span>
                </div>
                <select name="tipo" class="form-control @error('tipo') is-invalid @enderror" required>
                    <option value="">Escoja un tipo de mascota</option>
                    @forelse ($tipo_pacientes as $tipo_paciente)
                        <option value="{{ $tipo_paciente->Codigo }}" @if ($Paciente && $Paciente->Tipo == $tipo_paciente->Codigo){{ __('selected') }}@endif>{{ $tipo_paciente->Nombre }}</option>
                    @empty
                    @endforelse
                </select>
            </div>
            @error('tipo')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="col-12">
            @if ($Paciente)
            <div class="form-group">
                <button type="submit" class="btn float-right login_in_btn">
                    {{ __('Actualizar') }}
                </button>
                <a class="btn float-right login_in_btn mr-15" href="{{ route('pacientes') }}">
                    {{ __('Nuevo') }}
                </a>
            </div>
            @else
            <div class="form-group">
                <button type="submit" class="btn float-right login_in_btn">
                    {{ __('Guardar') }}
                </button>
            </div>
            @endif
        </div>
    </div>
</form>

<div class="row mt-20">
    <div class="col-sm-12">
        <table class="table table-condensed table-striped">
            <thead>
                <tr>
                    <th>Codigo</th>
                    <th>Nombre</th>
                    <th>Propietario</th>
                    <th>Direccion</th>
                    <th>Telefono</th>
                    <th>Fecha Nacimiento</th>
                    <th>Tipo</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($pacientes as $paciente)
                    <tr>
                        <td>{{ $paciente->Codigo }}</td>
                        <td>{{ $paciente->Nombre }}</td>
                        <td>{{ $paciente->Propietario }}</td>
                        <td>{{ $paciente->Direccion }}</td>
                        <td>{{ $paciente->Telefono }}</td>
                        <td>{{ $paciente->FechaNacimiento }}</td>
                        <td>{{ $paciente->tipo->Nombre }}</td>
                        <td>
                            <a class="btn btn-primary mr-15" href="{{ route('pacientes/edit', $paciente->Codigo) }}">
                                <span class="fas fa-edit mx-auto"></span>
                            </a>
                            <a class="btn btn-danger text-white" href="{{ route('pacientes/delete', $paciente->Codigo) }}">
                                <span class="fas fa-trash mx-auto"></span>
                            </a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="8">No hay registros.</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
</div>
@endsection
