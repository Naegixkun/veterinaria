@extends('layouts.appIn')

@section('content')

<form method="POST" action="@if ($Cita){{ route('citas/edit', $Cita->Numero) }}@else{{ route('citas') }}@endif">
    @csrf
    <div class="row">
        <div class="col-6">
            <div class="input-group form-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-dog mx-auto"></i></span>
                </div>
                <select name="codMascota" class="form-control @error('codMascota') is-invalid @enderror" required>
                    <option value="">Escoja una mascota</option>
                    @forelse ($mascotas as $mascota)
                        <option value="{{ $mascota->Codigo }}" @if ($Cita && $Cita->CodMascota == $mascota->Codigo){{ __('selected') }}@endif>{{ $mascota->Nombre }}</option>
                    @empty
                    @endforelse
                </select>
            </div>
            @error('codMascota')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="col-6">
            <div class="input-group form-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-user-md mx-auto"></i></span>
                </div>
                <select name="codVeterinario" class="form-control @error('codVeterinario') is-invalid @enderror" required>
                    <option value="">Escoja un veterinario</option>
                    @forelse ($veterinarios as $veterinario)
                        <option value="{{ $veterinario->Codigo }}" @if ($Cita && $Cita->CodVeterinario == $veterinario->Codigo){{ __('selected') }}@elseif($veterinarioEsc->CodVeterinario == $veterinario->Codigo){{ __('selected') }}@endif>{{ $veterinario->Nombres }} {{ $veterinario->Apellidos }}</option>
                    @empty
                    @endforelse
                </select>
            </div>
            @error('codVeterinario')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="col-6">
            <div class="input-group form-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-ambulance mx-auto"></i></span>
                </div>
                <select name="esEmergencia" class="form-control @error('esEmergencia') is-invalid @enderror" required>
                    <option value="">¿Es emergencia?</option>
                    <option value="1" @if ($Cita && $Cita->EsEmergencia == '1'){{ __('selected') }}@endif>Si</option>
                    <option value="0" @if ($Cita && $Cita->EsEmergencia == '0'){{ __('selected') }}@endif>No</option>
                </select>
            </div>
            @error('esEmergencia')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="col-12">
            @if ($Cita)
            <div class="form-group">
                <button type="submit" class="btn float-right login_in_btn">
                    {{ __('Actualizar') }}
                </button>
                <a class="btn float-right login_in_btn mr-15" href="{{ route('citas') }}">
                    {{ __('Nuevo') }}
                </a>
            </div>
            @else
            <div class="form-group">
                <button type="submit" class="btn float-right login_in_btn">
                    {{ __('Guardar') }}
                </button>
            </div>
            @endif
        </div>
    </div>
</form>

<div class="row mt-20">
    <div class="col-sm-12">
        <table class="table table-condensed table-striped">
            <thead>
                <tr>
                    <th>Numero</th>
                    <th>Mascota</th>
                    <th>Veterinario</th>
                    <th>Orden de espera</th>
                    <th>¿Es emergencia?</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($citas as $cita)
                    <tr>
                        <td>{{ $cita->Numero }}</td>
                        <td>{{ $cita->mascota->Nombre }}</td>
                        <td>{{ $cita->veterinario->Nombres }} {{ $cita->veterinario->Apellidos }}</td>
                        <td>{{ $cita->OrdenEspera }}</td>
                        <td>@if ($cita->EsEmergencia == '1'){{ __('Si') }}@else{{ __('No') }}@endif</td>
                        <td>
                            <a class="btn btn-primary mr-15" href="{{ route('citas/edit', $cita->Numero) }}">
                                <span class="fas fa-edit mx-auto"></span>
                            </a>
                            <a class="btn btn-danger text-white" href="{{ route('citas/delete', $cita->Numero) }}">
                                <span class="fas fa-trash mx-auto"></span>
                            </a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="8">No hay registros.</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
</div>
@if ($msg)
    <script>alert('{{ $msg }}')</script>
@endif
@endsection
