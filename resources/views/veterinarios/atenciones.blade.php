@extends('layouts.appIn')

@section('content')
@if ($Cita)
<form method="POST" action="{{ route('atenciones/save', $Cita->Numero) }}">
    @csrf
    <div class="row">
        <div class="col-6">
            <div class="input-group form-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-dog mx-auto"></i></span>
                </div>
                <select name="tipo" class="form-control @error('tipo') is-invalid @enderror" autofocus required>
                    <option value="">Tipo</option>
                    <option value="R">Recomendación</option>
                    <option value="M">Medicamento</option>
                </select>
            </div>
            @error('tipo')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="col-6">
            <div class="input-group form-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-pills mx-auto"></i></span>
                </div>
                <input type="number" name="cantidad" class="form-control @error('cantidad') is-invalid @enderror" placeholder="cantidad" required>  
            </div>
            @error('cantidad')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="col-6">
            <div class="input-group form-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-prescription-bottle mx-auto"></i></span>
                </div>
                <input type="text" name="posologia" class="form-control @error('posologia') is-invalid @enderror" placeholder="posologia" required>  
            </div>
            @error('posologia')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="col-12">
            <div class="input-group form-group">
                <textarea name="descripcion" placeholder="descripcion" rows="3" class="form-control @error('descripcion') is-invalid @enderror" required></textarea>
            </div>
            @error('descripcion')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="col-12">
            <div class="form-group">
                <button type="submit" class="btn float-right login_in_btn">
                    {{ __('Agregar Detalle') }}
                </button>
                <a href="{{ route('atenciones/ready', $Cita->Numero) }}" class="btn float-right login_in_btn mr-15">
                    {{ __('Terminar Atención') }}
                </a>
            </div>
        </div>
    </div>
</form>

<div class="row mt-20">
    <div class="col-sm-12">
        <table class="table table-condensed table-striped">
            <thead>
                <tr>
                    <th>Codigo</th>
                    <th>Tipo</th>
                    <th>Cantidad</th>
                    <th>Posologia</th>
                    <th>Descipcion</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($detalles as $detalle)
                    <tr>
                        <td>{{ $detalle->IDRegistro }}</td>
                        <td>@if ($detalle->Tipo == 'R'){{ __('Recomendación') }}@else{{ __('Medicamento') }}@endif</td>
                        <td>{{ $detalle->Cantidad }}</td>
                        <td>{{ $detalle->Posologia }}</td>
                        <td>{{ $detalle->Descripcion }}</td>
                        <td>
                            <a class="btn btn-danger text-white" href="{{ route('atenciones/delete', $detalle->IDRegistro) }}">
                                <span class="fas fa-trash mx-auto"></span>
                            </a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="6">No hay registros.</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
</div>
@endif

@if (!$Cita)
<div class="row mt-20">
    <div class="col-sm-12">
        <table class="table table-condensed table-striped">
            <thead>
                <tr>
                    <th>Numero</th>
                    <th>Mascota</th>
                    <th>Veterinario</th>
                    <th>Orden de espera</th>
                    <th>¿Es emergencia?</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($citas as $cita)
                    <tr>
                        <td>{{ $cita->Numero }}</td>
                        <td>{{ $cita->mascota->Nombre }}</td>
                        <td>{{ $cita->veterinario->Nombres }} {{ $cita->veterinario->Apellidos }}</td>
                        <td>{{ $cita->OrdenEspera }}</td>
                        <td>@if ($cita->EsEmergencia == '1'){{ __('Si') }}@else{{ __('No') }}@endif</td>
                        <td>
                            @if ($loop->first)
                            <a class="btn btn-success text-white" href="{{ route('atenciones/details', $cita->Numero) }}" >
                                <span class="fas fa-calendar-check mx-auto"></span>
                            </a>
                            @endif
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="8">No hay registros.</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
</div>
@endif

@if ($msg)
    <script>alert('{{ $msg }}')</script>
@endif
@endsection
