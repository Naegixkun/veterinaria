@extends('layouts.app')

@section('content')
<div class="d-flex justify-content-center h-100">
    <div class="card">
        <div class="card-header">
            <h3>{{ __('Iniciar Session') }}</h3>
        </div>
        <div class="card-body">
            <form method="POST" action="{{ route('login') }}">
                @csrf
                <div class="input-group form-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-user mx-auto"></i></span>
                    </div>
                    <input type="text" name="email" class="form-control @error('email') is-invalid @enderror" placeholder="correo" autofocus required autocomplete="email">  
                </div>
                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                <div class="input-group form-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-key mx-auto"></i></span>
                    </div>
                    <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" placeholder="contaseña" required autocomplete="current-password">
                </div>
                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                <div class="form-group">
                    <button type="submit" class="btn float-right login_btn">
                        {{ __('Ingresar') }}
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
