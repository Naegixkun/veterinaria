<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index');

Route::get('/admin/usuarios', 'usuariosController@index')->name('usuarios');
Route::post('/admin/usuarios', 'usuariosController@save');
Route::get('/admin/usuarios/edit/{id}', 'usuariosController@edit')->name('usuarios/edit');
Route::post('/admin/usuarios/edit/{id}', 'usuariosController@update');
Route::get('/admin/usuarios/delete/{id}', 'usuariosController@delete')->name('usuarios/delete');

Route::get('/admin/veterinarios', 'veterinariosController@index')->name('veterinarios');
Route::post('/admin/veterinarios', 'veterinariosController@save');
Route::get('/admin/veterinarios/edit/{id}', 'veterinariosController@edit')->name('veterinarios/edit');
Route::post('/admin/veterinarios/edit/{id}', 'veterinariosController@update');
Route::get('/admin/veterinarios/delete/{id}', 'veterinariosController@delete')->name('veterinarios/delete');

Route::get('/admin/pacientes', 'pacientesController@index')->name('pacientes');
Route::post('/admin/pacientes', 'pacientesController@save');
Route::get('/admin/pacientes/edit/{id}', 'pacientesController@edit')->name('pacientes/edit');
Route::post('/admin/pacientes/edit/{id}', 'pacientesController@update');
Route::get('/admin/pacientes/delete/{id}', 'pacientesController@delete')->name('pacientes/delete');

Route::get('/admin/citas', 'citasController@index')->name('citas');
Route::post('/admin/citas', 'citasController@save');
Route::get('/admin/citas/edit/{id}', 'citasController@edit')->name('citas/edit');
Route::post('/admin/citas/edit/{id}', 'citasController@update');
Route::get('/admin/citas/delete/{id}', 'citasController@delete')->name('citas/delete');

Route::get('/veterinario/atenciones', 'atencionesController@index')->name('atenciones');
Route::get('/veterinario/atenciones/details/{id}', 'atencionesController@details')->name('atenciones/details');
Route::post('/veterinario/atenciones/save/{id}', 'atencionesController@save')->name('atenciones/save');
Route::get('/veterinario/atenciones/ready/{id}', 'atencionesController@ready')->name('atenciones/ready');
Route::get('/veterinario/atenciones/delete/{id}', 'atencionesController@delete')->name('atenciones/delete');

Auth::routes();

