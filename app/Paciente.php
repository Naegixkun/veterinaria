<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paciente extends Model
{
    protected $fillable = [ 'nombre', 'propietario', 'direccion', 'telefono', 'fechaNacimiento', 'tipo'];
    protected $table = 'mascota';
    protected $primaryKey = 'Codigo';
    
    public function tipo()
    {
        return $this->hasOne('App\TipoPaciente', 'Codigo', 'Tipo');
    }
}
