<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class usuariosController extends Controller
{
    private $location_default = 'admin.usuarios';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    //informacion por default
    private function infoDefault($msg)
    {
        $usuarios = User::all();
        return [
            'msg' => $msg,
            'Usuario' => [],
            'usuarios' => $usuarios
        ];
    }

    /**
     * Show the application usuarios.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view($this->location_default, $this->infoDefault(''));
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(Request $request)
    {
        return Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' =>'required|string|email|max:255|unique:users',
            'rol' => 'required|string|max:1',
            'password' => 'required|string|min:8',
        ]);
    }

    /**
     * Insert the specified citas.
     *
     * @param  Request  $request
     * @return Response
     */
    public function save(Request $request)
    {
        $msg = 'No fué posible crear el usuario. Datos incompletos.';
        
        $name = $request->input('name', '');
        $email = $request->input('email', '');
        $rol = $request->input('rol', '');
        $password = $request->input('password', '');
        $password_confirmation = $request->input('password_confirmation', '');
        if ($password === $password_confirmation) {
            $validator = $this->validator($request);
            if (!$validator->fails()) {
                try {
                    $nuevo = new User;
                    $nuevo->name = $name;
                    $nuevo->email = $email;
                    $nuevo->rol = $rol;
                    $nuevo->password = Hash::make($password);
                    $nuevo->save();
    
                    $msg = 'La información se registró con éxito';
    
                } catch (Exception $e) {
                    $msg = $e->getMessage();
                }
            }
        } else {
            $msg = 'Verifique la contraseña';
        }

        return redirect()->route('usuarios');
    }
}