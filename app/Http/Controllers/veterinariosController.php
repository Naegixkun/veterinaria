<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Veterinario;
use App\User;

class veterinariosController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    //informacion por default
    private function infoDefault($msg)
    {
        $veterinarios = Veterinario::all()->where('estado', 1);
        $usuarios = User::all()->where('rol', 'V');
        return [
            'msg' => $msg,
            'Veterinario' => [],
            'veterinarios' => $veterinarios,
            'usuarios' => $usuarios
        ];
    }

    /**
     * Show the application veterinarios.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('admin.veterinarios', $this->infoDefault(''));
    }
    
    /**
     * Insert the specified veterinarios.
     *
     * @param  Request  $request
     * @return Response
     */
    public function save(Request $request)
    {
        $msg = 'No fué posible crear el veterinario. Datos incompletos.';
        
        $nombres = $request->input('nombres', '');
        $apellidos = $request->input('apellidos', '');
        $fechaNacimiento = $request->input('fechaNacimiento', '');
        $especialidad = $request->input('especialidad', '');
        $usuario = $request->input('user', '');

        if ($nombres && $apellidos && $fechaNacimiento && $especialidad && $usuario) {
            if ($nombres != "" && $apellidos != "" && $fechaNacimiento != "" && $especialidad != "" && $usuario != "") {
                try {
                    $nuevo = new Veterinario;
                    $nuevo->nombres = $nombres;
                    $nuevo->apellidos = $apellidos;
                    $nuevo->fechaNacimiento = $fechaNacimiento;
                    $nuevo->especialidad = $especialidad;
                    $nuevo->usuario = $usuario;
                    $nuevo->save();

                    $msg = 'La información se registró con éxito';

                } catch (Exception $e) {
                    $msg = $e->getMessage();
                }
            }
        }
        return redirect()->route('veterinarios');
    }

    /**
     * Edit the specified veterinario.
     *
     * @param  Request  $request
     * @param  string  $id
     * @return Response
     */
    public function edit(Request $request, $id)
    {
        $resp = $this->infoDefault('');
        $resp['Veterinario'] = Veterinario::find($id);
        return view('admin.veterinarios', $resp);
    }

    /**
     * Update the specified veterinario.
     *
     * @param  Request  $request
     * @param  string  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $msg = 'No fué posible actualizar el veterinario. Datos incompletos.';
        
        $nombres = $request->input('nombres', '');
        $apellidos = $request->input('apellidos', '');
        $fechaNacimiento = $request->input('fechaNacimiento', '');
        $especialidad = $request->input('especialidad', '');
        $usuario = $request->input('user', '');

        if ($nombres && $apellidos && $fechaNacimiento && $especialidad && $usuario) {
            if ($nombres != "" && $apellidos != "" && $fechaNacimiento != "" && $especialidad != "" && $usuario != "") {

                try {
                    $editar = Veterinario::find($id);
                    $editar->nombres = $nombres;
                    $editar->apellidos = $apellidos;
                    $editar->fechaNacimiento = $fechaNacimiento;
                    $editar->especialidad = $especialidad;
                    $editar->usuario = $usuario;
                    $editar->save();

                    $msg = 'La información se actualizo con éxito';

                } catch (Exception $e) {
                    $msg = $e->getMessage();
                }
            }
        }
        return redirect()->route('veterinarios');
    }

    /**
     * Delete the specified veterinario.
     *
     * @param  Request  $request
     * @param  string  $id
     * @return Response
     */
    public function delete(Request $request, $id)
    {        
        try {
            $editar = Veterinario::find($id);
            $editar->estado = 0;
            $editar->save();

            $msg = 'La información se actualizo con éxito';

        } catch (Exception $e) {
            $msg = $e->getMessage();
        }
        return redirect()->route('veterinarios');
    }
}
