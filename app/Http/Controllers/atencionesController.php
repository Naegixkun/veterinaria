<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Cita;
use App\DetalleCita;
use App\Veterinario;
use Illuminate\Support\Facades\Validator;

class atencionesController extends Controller
{
    private $location_default = 'veterinarios.atenciones';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    //informacion por default
    private function infoDefault($msg)
    {
        $veterinario = Veterinario::all()->where('usuario', Auth::id())->first();
        $citas = Cita::all()->where('CodVeterinario', $veterinario->Codigo)->where('estado', 1)->load('mascota')->load('veterinario')->sortBy('OrdenEspera')->sortByDesc('EsEmergencia');
        return [
            'msg' => $msg,
            'Cita' => [],
            'citas' => $citas,
            'detalles' => [],
        ];
    }

    /**
     * Show the application citas.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view($this->location_default, $this->infoDefault(''));
    }

    /**
     * Get a validator for an incoming data request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    private function validator(Request $request)
    {
        return Validator::make($request->all(), [
            'tipo' => 'required|max:1',
            'cantidad' => 'required|integer|max:1000',
            'posologia' => 'required|max:50',
            'descripcion' => 'required|max:255'
        ]);
    }

    /**
     * Insert the specified citas.
     *
     * @param  Request  $request
     * @return Response
     */
    public function save(Request $request, $id)
    {
        $msg = 'No fué posible crear el detalle de la cita. Datos incompletos.';
        
        $tipo = $request->input('tipo', '');
        $cantidad = $request->input('cantidad', '');
        $posologia = $request->input('posologia', '');
        $descripcion = $request->input('descripcion', '');

        $validator = $this->validator($request);
        if (!$validator->fails()) {
            try {
                $nuevo = new DetalleCita;
                $nuevo->NumAtencion = $id;
                $nuevo->Tipo = $tipo;
                $nuevo->Cantidad = $cantidad;
                $nuevo->Posologia = $posologia;
                $nuevo->Descripcion = $descripcion;
                $nuevo->save();

                $msg = 'La información se registró con éxito';

            } catch (Exception $e) {
                $msg = $e->getMessage();
            }
        }
        return redirect()->route('atenciones/details', $id);
    }

    /**
     * Ready the specified Cita.
     *
     * @param  Request  $request
     * @param  string  $id
     * @return Response
     */
    public function ready(Request $request, $id)
    {        
        try {
            $editar = Cita::find($id);
            $editar->estado = 2;
            $editar->save();

            $msg = 'La información se elimino con éxito';

        } catch (Exception $e) {
            $msg = $e->getMessage();
        }
        return redirect()->route('atenciones');
    }
    
    /**
     * Ready the specified Cita.
     *
     * @param  Request  $request
     * @param  string  $id
     * @return Response
     */
    public function details(Request $request, $id)
    {        
        $resp = $this->infoDefault('');
        $resp['Cita'] = Cita::find($id);
        $resp['detalles'] = DetalleCita::where([
            ['NumAtencion', '=', $id],
            ['estado', '=', 1]
        ])->get();
        return view($this->location_default, $resp);
    }

    /**
     * Delete the specified Cita.
     *
     * @param  Request  $request
     * @param  string  $id
     * @param  string  $origin
     * @return Response
     */
    public function delete(Request $request, $id)
    {        
        try {
            $editar = DetalleCita::find($id);
            $editar->estado = 0;
            $editar->save();

            $msg = 'La información se elimino con éxito';

        } catch (Exception $e) {
            $msg = $e->getMessage();
        }
        return redirect()->back();
    }
}
