<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Paciente;
use App\TipoPaciente;
use Illuminate\Support\Facades\Validator;

class pacientesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    //informacion por default
    private function infoDefault($msg)
    {
        $pacientes = Paciente::all()->where('estado', 1)->load('tipo');
        $tipo_pacientes = TipoPaciente::all()->where('estado', 1);
        return [
            'msg' => $msg,
            'Paciente' => [],
            'pacientes' => $pacientes,
            'tipo_pacientes' => $tipo_pacientes
        ];
    }

    /**
     * Show the application pacientes.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('admin.pacientes', $this->infoDefault(''));
    }
    
    /**
     * Insert the specified pacientes.
     *
     * @param  Request  $request
     * @return Response
     */
    public function save(Request $request)
    {
        $msg = 'No fué posible crear el Paciente. Datos incompletos.';
        
        $nombre = $request->input('nombre', '');
        $propietario = $request->input('propietario', '');
        $direccion = $request->input('direccion', '');
        $telefono = $request->input('telefono', '');
        $fechaNacimiento = $request->input('fechaNacimiento', '');
        $tipo = $request->input('tipo', '');

        $validator = $this->validator($request);
        if (!$validator->fails()) {
            try {
                $nuevo = new Paciente;
                $nuevo->nombre = $nombre;
                $nuevo->propietario = $propietario;
                $nuevo->direccion = $direccion;
                $nuevo->telefono = $telefono;
                $nuevo->fechaNacimiento = $fechaNacimiento;
                $nuevo->tipo = $tipo;
                $nuevo->save();

                $msg = 'La información se registró con éxito';

            } catch (Exception $e) {
                $msg = $e->getMessage();
            }
        }
        return redirect()->route('pacientes');
    }

    /**
     * Edit the specified Paciente.
     *
     * @param  Request  $request
     * @param  string  $id
     * @return Response
     */
    public function edit(Request $request, $id)
    {
        $resp = $this->infoDefault('');
        $resp['Paciente'] = Paciente::find($id);
        return view('admin.pacientes', $resp);
    }

    /**
     * Update the specified Paciente.
     *
     * @param  Request  $request
     * @param  string  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $msg = 'No fué posible actualizar el Paciente. Datos incompletos.';
        
        $nombre = $request->input('nombre', '');
        $propietario = $request->input('propietario', '');
        $direccion = $request->input('direccion', '');
        $telefono = $request->input('telefono', '');
        $fechaNacimiento = $request->input('fechaNacimiento', '');
        $tipo = $request->input('tipo', '');

        $validator = $this->validator($request);
        if (!$validator->fails()) {
            try {
                $editar = Paciente::find($id);
                $editar->nombre = $nombre;
                $editar->propietario = $propietario;
                $editar->direccion = $direccion;
                $editar->telefono = $telefono;
                $editar->fechaNacimiento = $fechaNacimiento;
                $editar->tipo = $tipo;
                $editar->save();

                $msg = 'La información se actualizo con éxito';

            } catch (Exception $e) {
                $msg = $e->getMessage();
            }
        }
        return redirect()->route('pacientes');
    }

    /**
     * Get a validator for an incoming data request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    private function validator(Request $request)
    {
        return Validator::make($request->all(), [
            'nombre' => 'required|max:255',
            'propietario' => 'required|max:255',
            'direccion' => 'required|max:255',
            'telefono' => 'required|max:10',
            'fechaNacimiento' => 'required|date',
            'tipo' => 'required|max:2',
        ]);
    }

    /**
     * Delete the specified Paciente.
     *
     * @param  Request  $request
     * @param  string  $id
     * @return Response
     */
    public function delete(Request $request, $id)
    {        
        try {
            $editar = Paciente::find($id);
            $editar->estado = 0;
            $editar->save();

            $msg = 'La información se elimino con éxito';

        } catch (Exception $e) {
            $msg = $e->getMessage();
        }
        return redirect()->route('pacientes');
    }
}
