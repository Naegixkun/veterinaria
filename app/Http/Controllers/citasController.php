<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Cita;
use App\Paciente;
use App\Veterinario;
use Illuminate\Support\Facades\Validator;

class citasController extends Controller
{
    private $location_default = 'admin.citas';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    //informacion por default
    private function infoDefault($msg)
    {
        $citas = Cita::all()->where('estado', 1)->load('mascota')->load('veterinario');
        $mascotas = Paciente::all()->where('estado', 1);
        $veterinarios = Veterinario::all()->where('estado', 1);
        $veterinario = Cita::select('CodVeterinario', \DB::raw('COUNT(Numero) as cantidad'))->where('estado', 1)->groupBy('CodVeterinario')->orderBy('cantidad', 'asc')->first();
        if (!$veterinario) $veterinario = Veterinario::select('Codigo', \DB::raw('Codigo as CodVeterinario'))->where('estado', 1)->first();
        return [
            'msg' => $msg,
            'Cita' => [],
            'citas' => $citas,
            'mascotas' => $mascotas,
            'veterinarios' => $veterinarios,
            'veterinarioEsc' => $veterinario
        ];
    }

    /**
     * Show the application citas.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view($this->location_default, $this->infoDefault(''));
    }
    
    /**
     * Insert the specified citas.
     *
     * @param  Request  $request
     * @return Response
     */
    public function save(Request $request)
    {
        $msg = 'No fué posible crear el Cita. Datos incompletos.';
        
        $codMascota = $request->input('codMascota', '');
        $codVeterinario = $request->input('codVeterinario', '');
        $esEmergencia = $request->input('esEmergencia', '');

        $validator = $this->validator($request);
        if (!$validator->fails()) {
            try {
                //buscar orden de espera por veterinario
                $cita_ult = Cita::where([
                    ['codVeterinario', '=', $codVeterinario]
                ])->orderBy('OrdenEspera', 'DESC')->first();
                
                $ordenEspera = ($cita_ult) ? (intval($cita_ult->OrdenEspera) + 1) : 1;
                //
                $nuevo = new Cita;
                $nuevo->codMascota = $codMascota;
                $nuevo->codVeterinario = $codVeterinario;
                $nuevo->ordenEspera = $ordenEspera;
                $nuevo->esEmergencia = $esEmergencia;
                $nuevo->save();

                $msg = 'La información se registró con éxito';

            } catch (Exception $e) {
                $msg = $e->getMessage();
            }
        }
        return redirect()->route('citas');
    }

    /**
     * Edit the specified Cita.
     *
     * @param  Request  $request
     * @param  string  $id
     * @return Response
     */
    public function edit(Request $request, $id)
    {
        $resp = $this->infoDefault('');
        $resp['Cita'] = Cita::find($id);
        return view($this->location_default, $resp);
    }

    /**
     * Update the specified Cita.
     *
     * @param  Request  $request
     * @param  string  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $msg = 'No fué posible actualizar el Cita. Datos incompletos.';

        $codMascota = $request->input('codMascota', '');
        $codVeterinario = $request->input('codVeterinario', '');
        $esEmergencia = $request->input('esEmergencia', '');

        $validator = $this->validator($request);
        if (!$validator->fails()) {
            try {
                $editar = Cita::find($id);
                $editar->codMascota = $codMascota;
                $editar->codVeterinario = $codVeterinario;
                $editar->esEmergencia = $esEmergencia;
                $editar->save();

                $msg = 'La información se actualizo con éxito';

            } catch (Exception $e) {
                $msg = $e->getMessage();
            }
        }
        return redirect()->route('citas');
    }

    /**
     * Get a validator for an incoming data request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    private function validator(Request $request)
    {
        return Validator::make($request->all(), [
            'codMascota' => 'required|integer|max:20',
            'codVeterinario' => 'required|max:20',
            'esEmergencia' => 'required|max:1'
        ]);
    }

    /**
     * Delete the specified Cita.
     *
     * @param  Request  $request
     * @param  string  $id
     * @return Response
     */
    public function delete(Request $request, $id)
    {        
        try {
            $editar = Cita::find($id);
            $editar->estado = 0;
            $editar->save();

            $msg = 'La información se elimino con éxito';

        } catch (Exception $e) {
            $msg = $e->getMessage();
        }
        return redirect()->route('citas');
    }
}