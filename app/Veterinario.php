<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Veterinario extends Model
{
    protected $fillable = [ 'nombres', 'apellidos', 'fechaNacimiento', 'especialidad', 'usuario'];
    protected $table = 'veterinario';
    protected $primaryKey = 'Codigo';
    
}
