<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoPaciente extends Model
{
    protected $fillable = [ 'nombre'];
    protected $table = 'tipo_mascota';
    protected $primaryKey = 'Codigo';

    public function paciente()
    {
        return $this->hasOne('App\Paciente', 'Tipo', 'Codigo');
    }
}
