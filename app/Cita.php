<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cita extends Model
{
    protected $fillable = [ 'codMascota', 'codVeterinario', 'ordenEspera', 'esEmergencia'];
    protected $table = 'atencion';
    protected $primaryKey = 'Numero';
    
    public function mascota()
    {
        return $this->hasOne('App\Paciente', 'Codigo', 'CodMascota');
    }

    public function veterinario()
    {
        return $this->hasOne('App\Veterinario', 'Codigo', 'CodVeterinario');
    }
}
