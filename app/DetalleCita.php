<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleCita extends Model
{
    protected $fillable = [ 'NumAtencion', 'Tipo', 'Descripcion', 'Cantidad', 'Posologia'];
    protected $table = 'detalle_atencion';
    protected $primaryKey = 'IDRegistro';
    
    public function mascota()
    {
        return $this->hasOne('App\Paciente', 'Codigo', 'CodMascota');
    }

    public function veterinario()
    {
        return $this->hasOne('App\Veterinario', 'Codigo', 'CodVeterinario');
    }
}
