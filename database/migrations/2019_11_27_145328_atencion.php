<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Atencion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('atencion');
        Schema::create('atencion', function (Blueprint $table) {
            $table->bigIncrements('Numero');
            $table->integer('CodMascota');
            $table->integer('CodVeterinario');
            $table->integer('OrdenEspera');
            $table->integer('EsEmergencia');
            $table->integer('estado')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('atencion');
    }
}
