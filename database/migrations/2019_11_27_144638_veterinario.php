<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Veterinario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('veterinario');
        Schema::create('veterinario', function (Blueprint $table) {
            $table->bigIncrements('Codigo');
            $table->char('Nombres', 50);
            $table->char('Apellidos', 50);
            $table->date('FechaNacimiento');
            $table->char('Especialidad', 50);
            $table->integer('usuario');
            $table->integer('estado')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('veterinario');
    }
}
