<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Mascota extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('mascota');
        Schema::create('mascota', function (Blueprint $table) {
            $table->bigIncrements('Codigo');
            $table->char('Nombre', 50);
            $table->char('Propietario', 100);
            $table->char('Direccion', 100);
            $table->char('Telefono', 20);
            $table->date('FechaNacimiento');
            $table->integer('Tipo');
            $table->integer('estado')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mascota');
    }
}
