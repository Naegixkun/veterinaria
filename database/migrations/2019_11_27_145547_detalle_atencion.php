<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DetalleAtencion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('detalle_atencion');
        Schema::create('detalle_atencion', function (Blueprint $table) {
            $table->bigIncrements('IDRegistro');
            $table->integer('NumAtencion');
            $table->char('Tipo', 1);
            $table->char('Descripcion', 200);
            $table->integer('Cantidad')->nullable();
            $table->char('Posologia', 50)->nullable();
            $table->integer('estado')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalle_atencion');
    }
}
